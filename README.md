# zm-PurgeWhenFull.sh

Remove old [Zone Minder](https://zoneminder.com/) events from a dedicated event drive when it gets full.

## Usage

You will need to modify the these config variables for this to work:
- mountpoint: this is the location where you zoneminder data drive is mounted.
- monitors: this is a space seperated list of zoneminder monitor numbers that you want to delete events from.
- start_pct: this is the percent capacity at which the script will start removeing old events.
- end_pct: this is the percent capacity at which the script will stop removeing old events.

This script is intended to be ran on a schedule via a systemd timer unit or cron job. I am running it every week using a sytemd timer unit but I don't see why it would not work with cron.

The script requires root privilages to delete events from zoneminder data drive.

## TODO

- Check for root privilages before running.
- Automaticly find all monitors in data drive.