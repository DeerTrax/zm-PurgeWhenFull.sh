#!/bin/sh

#zm-PurgeWhenFull.sh - Remove old Zone Minder events from a dedicated event drive when it gets full.
#Copyright (C) DeerTrax  2023

#This program is free software: you can redistribute it and/or modify it under the terms of
#the GNU General Public License as published by the Free Software Foundation, version
#3 of the License.

#This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#See the GNU General Public License for more details.

#You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

mountpoint=/zm_data #mountpoint of ZM event drive

monitors="1 3 4" #monitor numbers to remove events from

start_pct=95
end_pct=90
if [ $(id -u) -eq 0 ]; then
    get_current_pct() {
	#get current used precent from drive
	drive_pct=$(df $mountpoint | grep $mountpoint | grep -o "\w*%\w*" | tr -d %)
    }
    
    get_oldest_day() {
	unset oldest_days
	#get oldest day of events from each monitor
	for i in $monitors
	do
	    #-d makes will print whole path -1 1 item per line may not be requiered
	    oldest_days="$oldest_days $(ls -d1 $mountpoint/$i/* | head -n 1)"
	done
	echo oldest events from all monitors are $oldest_days
    }
    
    
    get_current_pct
    if [ $drive_pct -gt $start_pct ]
    then
	loops=0 #do I to init this variable?
	start_GB=$(df -h $mountpoint | grep $mountpoint | cut -d ' ' -f 10 | tr -d G) #record usage before removing anything
	while [ $drive_pct -gt $end_pct ]
	do
	    get_oldest_day  
	    rm -rfv $oldest_days #bin oldest days
	    get_current_pct #refresh used percent
	    loops=$[ $loops +1 ]
	done
	end_GB=$(df -h $mountpoint | grep $mountpoint | cut -d ' ' -f 10 | tr -d G) #usage at the end
	free_space=$[ $start_GB - $end_GB ] #calculate freed space
	echo Removed $loops days of events. Freeing ${free_space}GiB of space.
    else
	echo ZM drive is at $drive_pct%. The run threshold is $start_pct%. Exiting.
    fi	
    
else
    echo "This script must be run as root to work!"
    exit 1
fi
